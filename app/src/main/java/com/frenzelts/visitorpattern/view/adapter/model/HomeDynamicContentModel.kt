package com.frenzelts.visitorpattern.view.adapter.model

import com.frenzelts.visitorpattern.common.Visitable

data class HomeDynamicContentModel (
    val list: List<Visitable<*>> = listOf()
)