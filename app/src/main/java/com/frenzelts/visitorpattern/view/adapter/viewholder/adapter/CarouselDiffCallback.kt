package com.frenzelts.visitorpattern.view.adapter.viewholder.adapter

import androidx.recyclerview.widget.DiffUtil
import com.frenzelts.visitorpattern.common.Visitable
import com.frenzelts.visitorpattern.view.adapter.model.dynamic_content.CarouselVisitable
import com.frenzelts.visitorpattern.view.adapter.model.dynamic_content.HomeVisitable

object CarouselDiffCallback : DiffUtil.ItemCallback<Visitable<*>>() {
    override fun areItemsTheSame(oldItem: Visitable<*>, newItem: Visitable<*>): Boolean {
        if (oldItem is CarouselVisitable && newItem is CarouselVisitable) {
            return oldItem.visitableId() == newItem.visitableId()
        }

        return false
    }

    override fun areContentsTheSame(oldItem: Visitable<*>, newItem: Visitable<*>): Boolean {
        if (oldItem is CarouselVisitable && newItem is CarouselVisitable) {
            return oldItem.equalsWith(newItem)
        }

        return false
    }
}