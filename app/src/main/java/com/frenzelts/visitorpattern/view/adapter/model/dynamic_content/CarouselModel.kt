package com.frenzelts.visitorpattern.view.adapter.model.dynamic_content

import android.os.Bundle
import com.frenzelts.visitorpattern.data.model.ContentModel
import com.frenzelts.visitorpattern.view.adapter.typefactory.HomeTypeFactory

class CarouselModel(
    val contentModel: ContentModel
): HomeVisitable {
    override fun visitableId(): String {
        return contentModel.id
    }

    override fun equalsWith(b: Any?): Boolean {
        return if (b is CarouselModel) {
            b == this
        } else false
    }

    override fun getChangePayloadFrom(b: Any?): Bundle {
        return Bundle()
    }

    override fun type(typeFactory: HomeTypeFactory): Int {
        return typeFactory.type(this)
    }
}