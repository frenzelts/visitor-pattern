package com.frenzelts.visitorpattern.view.adapter.typefactory

import android.util.Log
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.frenzelts.visitorpattern.common.BaseTypeFactoryImpl
import com.frenzelts.visitorpattern.common.BaseViewHolder
import com.frenzelts.visitorpattern.view.adapter.model.dynamic_content.CarouselModel
import com.frenzelts.visitorpattern.view.adapter.model.dynamic_content.Grid4Model
import com.frenzelts.visitorpattern.view.adapter.viewholder.CarouselViewHolder
import com.frenzelts.visitorpattern.view.adapter.viewholder.Grid4ViewHolder

class HomeTypeFactoryImpl(
    private val parentRecycledViewPool: RecyclerView.RecycledViewPool? = null
) : BaseTypeFactoryImpl(), HomeTypeFactory {
    override fun type(grid4Model: Grid4Model): Int {
        return Grid4ViewHolder.LAYOUT
    }

    override fun type(carouselModel: CarouselModel): Int {
        return CarouselViewHolder.LAYOUT
    }

    override fun createViewHolder(view: View, viewType: Int): BaseViewHolder<*> {
        return when(viewType){
            Grid4ViewHolder.LAYOUT -> {
                Grid4ViewHolder(view, parentRecycledViewPool)
            }
            CarouselViewHolder.LAYOUT -> {
                CarouselViewHolder(view, parentRecycledViewPool)
            }
            else -> {
                super.createViewHolder(view, viewType)
            }
        }
    }
}