package com.frenzelts.visitorpattern.view.adapter.typefactory

import com.frenzelts.visitorpattern.view.adapter.model.dynamic_content.CarouselModel
import com.frenzelts.visitorpattern.view.adapter.model.dynamic_content.Grid4Model

interface HomeTypeFactory {
    fun type(grid4Model: Grid4Model): Int = 0
    fun type(carouselModel: CarouselModel): Int = 0
}