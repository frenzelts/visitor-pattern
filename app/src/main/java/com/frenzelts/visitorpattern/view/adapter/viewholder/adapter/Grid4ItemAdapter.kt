package com.frenzelts.visitorpattern.view.adapter.viewholder.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.frenzelts.visitorpattern.R
import com.frenzelts.visitorpattern.data.model.ContentModel

class Grid4ItemAdapter(private val content: ContentModel)
    : RecyclerView.Adapter<Grid4ItemAdapter.GridItemViewHolder>(){

    companion object {
        @LayoutRes
        private val GRID_ITEM = R.layout.dc_grid4_item
    }

    class GridItemViewHolder(view: View): RecyclerView.ViewHolder(view){
        val image: ImageView by lazy { view.findViewById(R.id.image) }
        val name: TextView by lazy { view.findViewById(R.id.name) }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GridItemViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(viewType, parent, false)
        return GridItemViewHolder(v)
    }

    override fun getItemViewType(position: Int): Int {
        return GRID_ITEM
    }

    override fun onBindViewHolder(holder: GridItemViewHolder, position: Int) {
        val item = content.contentItems[position]
        Glide.with(holder.itemView.context)
            .load(item.imageUrl)
            .into(holder.image)
        holder.name.text = item.name
    }

    override fun getItemCount(): Int {
        return content.contentItems.size
    }
}