package com.frenzelts.visitorpattern.view.adapter.model.dynamic_content

import com.frenzelts.visitorpattern.data.model.ContentItem
import com.frenzelts.visitorpattern.data.model.ContentModel
import com.frenzelts.visitorpattern.view.adapter.typefactory.CarouselTypeFactory

class CarouselViewAllCardModel: CarouselVisitable {
    override fun visitableId(): String {
        return ""
    }

    override fun equalsWith(b: Any?): Boolean {
        return if (b is CarouselViewAllCardModel) {
            b == this
        } else false
    }

    override fun type(typeFactory: CarouselTypeFactory): Int {
        return typeFactory.type(this)
    }
}