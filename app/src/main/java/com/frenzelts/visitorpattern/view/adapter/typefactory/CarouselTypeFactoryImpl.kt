package com.frenzelts.visitorpattern.view.adapter.typefactory

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.frenzelts.visitorpattern.common.BaseTypeFactoryImpl
import com.frenzelts.visitorpattern.common.BaseViewHolder
import com.frenzelts.visitorpattern.data.model.ContentItem
import com.frenzelts.visitorpattern.view.adapter.model.dynamic_content.CarouselProductCardModel
import com.frenzelts.visitorpattern.view.adapter.model.dynamic_content.CarouselViewAllCardModel
import com.frenzelts.visitorpattern.view.adapter.viewholder.CarouselProductCardViewHolder
import com.frenzelts.visitorpattern.view.adapter.viewholder.CarouselViewAllCardViewHolder

class CarouselTypeFactoryImpl(
    private val parentRecycledViewPool: RecyclerView.RecycledViewPool?
) : BaseTypeFactoryImpl(), CarouselTypeFactory {

    override fun type(model: CarouselProductCardModel): Int {
        return CarouselProductCardViewHolder.LAYOUT
    }

    override fun type(model: CarouselViewAllCardModel): Int {
        return CarouselViewAllCardViewHolder.LAYOUT
    }

    override fun createViewHolder(view: View, viewType: Int): BaseViewHolder<*> {
        return when(viewType){
            CarouselViewAllCardViewHolder.LAYOUT -> {
                CarouselViewAllCardViewHolder(view)
            }
            CarouselProductCardViewHolder.LAYOUT -> {
                CarouselProductCardViewHolder(view)
            }
            else -> super.createViewHolder(view, viewType)
        }
    }
}