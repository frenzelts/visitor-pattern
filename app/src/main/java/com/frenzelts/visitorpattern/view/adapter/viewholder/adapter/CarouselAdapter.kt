package com.frenzelts.visitorpattern.view.adapter.viewholder.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncDifferConfig
import androidx.recyclerview.widget.ListAdapter
import com.frenzelts.visitorpattern.common.BaseViewHolder
import com.frenzelts.visitorpattern.common.Visitable
import com.frenzelts.visitorpattern.view.adapter.typefactory.CarouselTypeFactoryImpl

class CarouselAdapter(
    asyncDifferConfig: AsyncDifferConfig<Visitable<*>>,
    private val carouselTypeFactoryImpl: CarouselTypeFactoryImpl
) : ListAdapter<Visitable<*>, BaseViewHolder<Visitable<*>>>(asyncDifferConfig) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<Visitable<*>> {
        val view = LayoutInflater.from(parent.context).inflate(viewType, parent, false)
        return carouselTypeFactoryImpl.createViewHolder(view, viewType) as BaseViewHolder<Visitable<*>>
    }

    override fun onBindViewHolder(holder: BaseViewHolder<Visitable<*>>, position: Int) {
        holder.bind(getItem(position))
    }

    override fun onBindViewHolder(
        holder: BaseViewHolder<Visitable<*>>,
        position: Int,
        payloads: MutableList<Any>
    ) {
        holder.bind(getItem(position), payloads)
    }

    override fun getItemViewType(position: Int): Int {
        return (getItem(position) as Visitable<CarouselTypeFactoryImpl>).type(carouselTypeFactoryImpl)
    }
}