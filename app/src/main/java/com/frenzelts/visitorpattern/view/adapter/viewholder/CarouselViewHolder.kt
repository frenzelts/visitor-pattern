package com.frenzelts.visitorpattern.view.adapter.viewholder

import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.AsyncDifferConfig
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.frenzelts.visitorpattern.R
import com.frenzelts.visitorpattern.common.BaseViewHolder
import com.frenzelts.visitorpattern.common.Visitable
import com.frenzelts.visitorpattern.data.model.ContentModel
import com.frenzelts.visitorpattern.view.adapter.HomeDiffCallback
import com.frenzelts.visitorpattern.view.adapter.model.dynamic_content.CarouselModel
import com.frenzelts.visitorpattern.view.adapter.model.dynamic_content.CarouselProductCardModel
import com.frenzelts.visitorpattern.view.adapter.model.dynamic_content.CarouselViewAllCardModel
import com.frenzelts.visitorpattern.view.adapter.typefactory.CarouselTypeFactoryImpl
import com.frenzelts.visitorpattern.view.adapter.viewholder.adapter.CarouselAdapter
import java.util.concurrent.Executors

class CarouselViewHolder(
    itemView: View,
    private val parentRecyclerViewPool: RecyclerView.RecycledViewPool? = null,
): BaseViewHolder<CarouselModel>(itemView) {

    private lateinit var adapter: CarouselAdapter
    private val recyclerView : RecyclerView by lazy {
        itemView.findViewById(R.id.rv_carousel)
    }

    private val title : TextView by lazy {
        itemView.findViewById(R.id.title)
    }

     companion object{
         @LayoutRes
         val LAYOUT = R.layout.dc_carousel_holder
     }

    override fun bind(element: CarouselModel) {
        setHeader(element.contentModel)
        setupRecyclerView()
        populateList(element)
    }

    override fun bind(element: CarouselModel, payloads: List<Any?>) {
        bind(element)
    }

    private fun setHeader(content: ContentModel){
        title.text = content.contentHeader.name
    }

    private fun setupRecyclerView(){
        parentRecyclerViewPool?.let {
            recyclerView.setRecycledViewPool(parentRecyclerViewPool)
        }

        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = LinearLayoutManager(
            itemView.context,
            LinearLayoutManager.HORIZONTAL,
            false
        )

        adapter = CarouselAdapter(
            AsyncDifferConfig.Builder(HomeDiffCallback)
                .setBackgroundThreadExecutor(Executors.newSingleThreadExecutor())
                .build(),
            CarouselTypeFactoryImpl(parentRecyclerViewPool)
        )
        recyclerView.adapter = adapter
    }

    private fun populateList(element: CarouselModel){
        val listData = mutableListOf<Visitable<*>>()
        listData.addAll(
            element.contentModel.contentItems.map {
                CarouselProductCardModel(it)
            }
        )
        listData.add(CarouselViewAllCardModel())
        adapter.submitList(listData)
    }
}