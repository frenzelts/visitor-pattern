package com.frenzelts.visitorpattern.view.adapter.viewholder

import android.view.View
import android.widget.TextView
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.frenzelts.visitorpattern.R
import com.frenzelts.visitorpattern.common.BaseViewHolder
import com.frenzelts.visitorpattern.data.model.ContentModel
import com.frenzelts.visitorpattern.view.adapter.model.dynamic_content.Grid4Model
import com.frenzelts.visitorpattern.view.adapter.viewholder.adapter.Grid4ItemAdapter

class Grid4ViewHolder(
    itemView: View,
    private val parentRecyclerViewPool: RecyclerView.RecycledViewPool? = null,
): BaseViewHolder<Grid4Model>(itemView) {

    companion object{
        @LayoutRes
        val LAYOUT = R.layout.dc_grid4_holder
        const val SPAN_COUNT = 2
    }

    private val recyclerView : RecyclerView by lazy {
        itemView.findViewById(R.id.rv_grid4)
    }

    private val title : TextView by lazy {
        itemView.findViewById(R.id.title)
    }

    override fun bind(element: Grid4Model) {
        setHeader(element.contentModel)
        setItems(element.contentModel)
    }

    override fun bind(element: Grid4Model, payloads: List<Any?>) {
        bind(element)
    }

    private fun setHeader(content: ContentModel){
        title.text = content.contentHeader.name
    }

    private fun setItems(content: ContentModel){
        parentRecyclerViewPool?.let {
            recyclerView.setRecycledViewPool(parentRecyclerViewPool)
        }

        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = GridLayoutManager(
            itemView.context,
            SPAN_COUNT,
            GridLayoutManager.VERTICAL,
            false
        )

        recyclerView.adapter = Grid4ItemAdapter(content)
    }
}