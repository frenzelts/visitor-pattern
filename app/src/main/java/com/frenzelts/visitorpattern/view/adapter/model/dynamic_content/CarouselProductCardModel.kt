package com.frenzelts.visitorpattern.view.adapter.model.dynamic_content

import android.os.Bundle
import com.frenzelts.visitorpattern.common.Visitable
import com.frenzelts.visitorpattern.data.model.ContentItem
import com.frenzelts.visitorpattern.data.model.ContentModel
import com.frenzelts.visitorpattern.view.adapter.typefactory.CarouselTypeFactory
import com.frenzelts.visitorpattern.view.adapter.typefactory.HomeTypeFactory

class CarouselProductCardModel(
    val item: ContentItem
): CarouselVisitable {
    override fun visitableId(): String {
        return item.id
    }

    override fun equalsWith(b: Any?): Boolean {
        return if (b is CarouselProductCardModel) {
            b == this
        } else false
    }

    override fun type(typeFactory: CarouselTypeFactory): Int {
        return typeFactory.type(this)
    }
}