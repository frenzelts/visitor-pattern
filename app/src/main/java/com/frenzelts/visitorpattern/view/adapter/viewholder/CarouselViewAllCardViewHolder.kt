package com.frenzelts.visitorpattern.view.adapter.viewholder

import android.view.View
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.RecyclerView
import com.frenzelts.visitorpattern.R
import com.frenzelts.visitorpattern.common.BaseViewHolder
import com.frenzelts.visitorpattern.view.adapter.model.dynamic_content.CarouselModel
import com.frenzelts.visitorpattern.view.adapter.model.dynamic_content.CarouselViewAllCardModel

class CarouselViewAllCardViewHolder(
    itemView: View
): BaseViewHolder<CarouselViewAllCardModel>(itemView) {

     companion object{
         @LayoutRes
         val LAYOUT = R.layout.view_all_card
     }

    override fun bind(element: CarouselViewAllCardModel) {

    }

    override fun bind(element: CarouselViewAllCardModel, payloads: List<Any?>) {
        bind(element)
    }
}