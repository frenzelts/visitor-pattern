package com.frenzelts.visitorpattern.view

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.frenzelts.visitorpattern.data.repository.HomeDynamicContentRepository
import com.frenzelts.visitorpattern.view.adapter.model.HomeDynamicContentModel

class HomeViewModel (
    val repository: HomeDynamicContentRepository
) : ViewModel(){

    val homeLiveDynamicContent: LiveData<HomeDynamicContentModel>
        get() = _homeLiveDynamicChannel
    private val _homeLiveDynamicChannel: MutableLiveData<HomeDynamicContentModel> = MutableLiveData()

    fun fetchDynamicContent(){
        _homeLiveDynamicChannel.postValue(HomeDynamicContentModel(repository.fetchDynamicContent()))
    }
}