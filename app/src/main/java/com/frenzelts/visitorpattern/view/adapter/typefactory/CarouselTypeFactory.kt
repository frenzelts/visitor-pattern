package com.frenzelts.visitorpattern.view.adapter.typefactory

import com.frenzelts.visitorpattern.data.model.ContentItem
import com.frenzelts.visitorpattern.view.adapter.model.dynamic_content.CarouselProductCardModel
import com.frenzelts.visitorpattern.view.adapter.model.dynamic_content.CarouselViewAllCardModel

interface CarouselTypeFactory {
    fun type(model: CarouselProductCardModel): Int = 0
    fun type(model: CarouselViewAllCardModel): Int = 0
}