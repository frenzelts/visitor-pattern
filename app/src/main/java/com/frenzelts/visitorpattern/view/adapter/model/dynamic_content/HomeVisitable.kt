package com.frenzelts.visitorpattern.view.adapter.model.dynamic_content

import android.os.Bundle
import com.frenzelts.visitorpattern.common.Visitable
import com.frenzelts.visitorpattern.view.adapter.typefactory.HomeTypeFactory

interface HomeVisitable: Visitable<HomeTypeFactory> {
    fun visitableId(): String?
    fun equalsWith(b: Any?): Boolean
    fun getChangePayloadFrom(b: Any?): Bundle?
}