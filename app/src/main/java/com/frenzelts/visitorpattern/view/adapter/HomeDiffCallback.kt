package com.frenzelts.visitorpattern.view.adapter

import androidx.recyclerview.widget.DiffUtil
import com.frenzelts.visitorpattern.common.Visitable
import com.frenzelts.visitorpattern.view.adapter.model.dynamic_content.HomeVisitable

object HomeDiffCallback : DiffUtil.ItemCallback<Visitable<*>>() {
    override fun areItemsTheSame(oldItem: Visitable<*>, newItem: Visitable<*>): Boolean {
        if (oldItem is HomeVisitable && newItem is HomeVisitable) {
            return oldItem.visitableId() == newItem.visitableId()
        }

        return false
    }

    override fun areContentsTheSame(oldItem: Visitable<*>, newItem: Visitable<*>): Boolean {
        if (oldItem is HomeVisitable && newItem is HomeVisitable) {
            return oldItem.equalsWith(newItem)
        }

        return false
    }
}