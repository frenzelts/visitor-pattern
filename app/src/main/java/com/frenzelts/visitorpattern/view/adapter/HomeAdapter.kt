package com.frenzelts.visitorpattern.view.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncDifferConfig
import androidx.recyclerview.widget.ListAdapter
import com.frenzelts.visitorpattern.common.BaseViewHolder
import com.frenzelts.visitorpattern.common.Visitable
import com.frenzelts.visitorpattern.view.adapter.typefactory.HomeTypeFactory
import com.frenzelts.visitorpattern.view.adapter.typefactory.HomeTypeFactoryImpl

class HomeAdapter(
    asyncDifferConfig: AsyncDifferConfig<Visitable<*>>,
    private val homeTypeFactoryImpl: HomeTypeFactoryImpl
) : ListAdapter<Visitable<*>, BaseViewHolder<Visitable<*>>>(asyncDifferConfig) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<Visitable<*>> {
        val view = LayoutInflater.from(parent.context).inflate(viewType, parent, false)
        return homeTypeFactoryImpl.createViewHolder(view, viewType) as BaseViewHolder<Visitable<*>>
    }

    override fun onBindViewHolder(holder: BaseViewHolder<Visitable<*>>, position: Int) {
        holder.bind(getItem(position))
    }

    override fun onBindViewHolder(
        holder: BaseViewHolder<Visitable<*>>,
        position: Int,
        payloads: MutableList<Any>
    ) {
        holder.bind(getItem(position), payloads)
    }

    override fun getItemViewType(position: Int): Int {
        return (getItem(position) as Visitable<HomeTypeFactory>).type(homeTypeFactoryImpl)
    }
}