package com.frenzelts.visitorpattern.view.adapter.viewholder

import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.frenzelts.visitorpattern.R
import com.frenzelts.visitorpattern.common.BaseViewHolder
import com.frenzelts.visitorpattern.view.adapter.model.dynamic_content.CarouselModel
import com.frenzelts.visitorpattern.view.adapter.model.dynamic_content.CarouselProductCardModel

class CarouselProductCardViewHolder(
    itemView: View
): BaseViewHolder<CarouselProductCardModel>(itemView) {

     companion object{
         @LayoutRes
         val LAYOUT = R.layout.dc_carousel_product_card
     }

    private val image: ImageView by lazy { itemView.findViewById(R.id.image) }
    private val name: TextView by lazy { itemView.findViewById(R.id.product_name) }
    private val code: TextView by lazy { itemView.findViewById(R.id.product_code) }
    private val price: TextView by lazy { itemView.findViewById(R.id.product_price) }

    override fun bind(element: CarouselProductCardModel) {
        val item = element.item
        Glide.with(itemView.context)
            .load(item.imageUrl)
            .into(image)
        name.text = item.name
        code.text = "Code: ${item.id}"
        price.text = item.price
    }

    override fun bind(element: CarouselProductCardModel, payloads: List<Any?>) {
        bind(element)
    }
}