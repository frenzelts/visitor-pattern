//package com.frenzelts.visitorpattern.di
//
//import android.content.Context
//import com.frenzelts.visitorpattern.MainActivity
//import dagger.Component
//import javax.inject.Scope
//
//@Scope
//@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
//annotation class HomeScope
//
//@HomeScope
//@Component(modules = [HomeViewModelModule::class], dependencies = [BaseAppComponent::class])
//interface HomeComponent {
//    @ApplicationContext
//    fun getApplicationContext(): Context
//    fun inject(mainActivity: MainActivity)
//}