//package com.frenzelts.visitorpattern.di
//
//import androidx.lifecycle.ViewModel
//import androidx.lifecycle.ViewModelProvider
//import com.frenzelts.visitorpattern.view.HomeViewModel
//import dagger.Binds
//import dagger.MapKey
//import dagger.Module
//import dagger.Provides
//import dagger.multibindings.IntoMap
//import kotlin.reflect.KClass
//
//@Module
//abstract class HomeViewModelModule {
//    @Binds
//    internal abstract fun bindViewModelFactory(viewModelFactory: ViewModelFactory): ViewModelProvider.Factory
//
//    @Binds
//    @IntoMap
//    @ViewModelKey(HomeViewModel::class)
//    internal abstract fun provideHomeViewModel(viewModel: HomeViewModel) : ViewModel
//
//}
//
//@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER, AnnotationTarget.PROPERTY_SETTER)
//@Retention(AnnotationRetention.RUNTIME)
//@MapKey
//annotation class ViewModelKey(val value: KClass<out ViewModel>)