//package com.frenzelts.visitorpattern.di
//
//import android.content.Context
//import dagger.Module
//import dagger.Provides
//import java.lang.annotation.Retention
//import java.lang.annotation.RetentionPolicy
//import javax.inject.Qualifier
//import javax.inject.Scope
//
//@Module
//class AppModule(private val context: Context) {
//    @ApplicationScope
//    @Provides
//    @ApplicationContext
//    fun provideContext(): Context {
//        return context.applicationContext
//    }
//}
//
//@Scope
//@Retention(RetentionPolicy.CLASS)
//annotation class ApplicationScope
//
//@Qualifier
//annotation class ApplicationContext
