package com.frenzelts.visitorpattern.common

class TypeNotSupportedException private constructor(message: String) : RuntimeException(message) {
    companion object {
        fun create(message: String): TypeNotSupportedException {
            return TypeNotSupportedException(message)
        }
    }
}
