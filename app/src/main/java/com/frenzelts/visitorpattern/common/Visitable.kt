package com.frenzelts.visitorpattern.common

interface Visitable<T> {
    fun type(typeFactory: T): Int
}