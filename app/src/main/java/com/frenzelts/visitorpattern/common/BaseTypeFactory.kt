package com.frenzelts.visitorpattern.common

import android.view.View

interface BaseTypeFactory {
//    fun type(model: EmptyModel): Int
//    fun type(model: LoadingModel): Int
//    fun type(model: LoadingMoreModel): Int
//    fun type(model: ErrorNetworkModel): Int
//    fun type(model: EmptyResultViewModel): Int
    fun createViewHolder(parent: View, type: Int): BaseViewHolder<*>
}