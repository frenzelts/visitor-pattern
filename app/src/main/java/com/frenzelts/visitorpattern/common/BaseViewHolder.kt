package com.frenzelts.visitorpattern.common

import android.view.View
import androidx.recyclerview.widget.RecyclerView

abstract class BaseViewHolder<T : Visitable<*>>(itemView: View)
    : RecyclerView.ViewHolder(itemView) {
    abstract fun bind(element: T)
    open fun bind(element: T, payloads: List<Any?>) {}
}