package com.frenzelts.visitorpattern.common

import android.view.View

open class BaseTypeFactoryImpl: BaseTypeFactory {

//    fun type(viewModel: EmptyModel?): Int {
//        return EmptyViewHolder.LAYOUT
//    }
//
//    fun type(viewModel: EmptyResultViewModel?): Int {
//        return EmptyResultViewHolder.LAYOUT
//    }
//
//    fun type(viewModel: ErrorNetworkModel?): Int {
//        return ErrorNetworkViewHolder.LAYOUT
//    }
//
//    fun type(viewModel: LoadingModel?): Int {
//        return LoadingViewholder.LAYOUT
//    }
//
//    fun type(viewModel: LoadingMoreModel?): Int {
//        return LoadingMoreViewHolder.LAYOUT
//    }

    override fun createViewHolder(view: View, viewType: Int): BaseViewHolder<*> {
//        val viewHolder: BaseViewHolder<*>
//        if (type == LoadingMoreViewHolder.LAYOUT) {
//            viewHolder = LoadingMoreViewHolder(parent)
//        } else if (type == LoadingViewholder.LAYOUT) {
//            viewHolder = LoadingViewholder(parent)
//        } else if (type == ErrorNetworkViewHolder.LAYOUT) {
//            viewHolder = ErrorNetworkViewHolder(parent)
//        } else if (type == EmptyResultViewHolder.LAYOUT) {
//            viewHolder = EmptyResultViewHolder(parent)
//        } else if (type == EmptyViewHolder.LAYOUT) {
//            viewHolder = EmptyViewHolder(parent)
//        } else if (type == HideViewHolder.LAYOUT) {
//            viewHolder = HideViewHolder(parent)
//        } else {
//            throw TypeNotSupportedException.create("Layout not supported")
//        }
        throw TypeNotSupportedException.create("Layout not supported")
    }
}