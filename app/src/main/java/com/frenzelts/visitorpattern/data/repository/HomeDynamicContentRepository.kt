package com.frenzelts.visitorpattern.data.repository

import android.app.Application
import android.util.Log
import com.frenzelts.visitorpattern.R
import com.frenzelts.visitorpattern.common.Visitable
import com.frenzelts.visitorpattern.data.mapper.HomeDynamicContentMapper
import com.frenzelts.visitorpattern.data.model.Data
import com.frenzelts.visitorpattern.data.model.HomeContentsData
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.io.InputStreamReader
import java.lang.reflect.Type

class HomeDynamicContentRepository (
    val context: Application
) {

    fun fetchDynamicContent() : List<Visitable<*>>{
        return HomeDynamicContentMapper()
            .mapDynamicContentToVisitableList(
                readJson().dynamicHomeContent.contents
            )
    }

    private fun readJson() : HomeContentsData {
        val stream = context.resources.openRawResource(R.raw.response_mock_data_dynamic_content)
        val reader = InputStreamReader(stream)
        val type: Type = object : TypeToken<Data<HomeContentsData>>() {}.type
        val model = Gson().fromJson<Data<HomeContentsData>>(reader, type).data
        reader.close()
        stream.close()
        return model
    }

}