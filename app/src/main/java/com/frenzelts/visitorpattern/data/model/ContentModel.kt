package com.frenzelts.visitorpattern.data.model

data class ContentModel(
    val id: String,
    val groupId: String,
    val type: String = "",
    var verticalPosition: Int = 0,
    var contentHeader: ContentHeader = ContentHeader(),
    val contentBanner: ContentBanner = ContentBanner(),
    val contentItems: List<ContentItem> = listOf(),
    val name : String = "",
    val layout: String = ""
)

data class ContentItem(
    val id: String = "",
    val price: String = "0",
    val imageUrl: String = "",
    val name: String = "",
    val applink: String = "",
    val discount: String = "",
    val slashedPrice: String = "",
    val backColor: String = "",
    val textColor: String = "",
)

data class ContentBanner(
    val id: String = "",
    val title: String = "",
    val description: String = "",
    val backColor: String = "",
    val applink: String = "",
    val textColor: String = "",
    val imageUrl: String = "",
    val gradientColor: ArrayList<String> = arrayListOf("")
)

data class ContentHeader(
    val id: String = "",
    val name: String = "",
    val subtitle: String = "",
    val applink: String = "",
    val backColor: String = "",
    val backImage: String = "",
    val textColor: String = ""
)