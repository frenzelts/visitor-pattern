package com.frenzelts.visitorpattern.data.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class HomeContentsData(
    @SerializedName("homeDynamicContents")
    val dynamicHomeContent: DynamicHomeContent = DynamicHomeContent()
)

data class DynamicHomeContent(
    @Expose
    @SerializedName("contents")
    var contents: List<Content> = listOf()
) {

    data class Content(
        @Expose
        @SerializedName("id")
        val id: String = "",
        @Expose
        @SerializedName("group_id", alternate = ["groupId"])
        val groupId: String = "",
        @Expose
        @SerializedName("layout")
        val layout: String = "",
        @Expose
        @SerializedName("name")
        val name: String = "",
        @Expose
        @SerializedName("items")
        val items: Array<Item> = arrayOf(),
        @Expose
        @SerializedName("type")
        val type: String = "",
        @Expose
        @SerializedName("header")
        val header: Header = Header(),
        @Expose
        @SerializedName("banner")
        val banner: Banner = Banner(),
        @SerializedName("hasCloseButton")
        @Expose
        val hasCloseButton: Boolean = false
    ){
        companion object{
            const val LAYOUT_GRID4 = "grid_4"
            const val LAYOUT_CAROUSEL = "top_carousel"
        }

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false

            other as Content

            if (id != other.id) return false
            if (groupId != other.groupId) return false
            if (layout != other.layout) return false
            if (name != other.name) return false
            if (!items.contentEquals(other.items)) return false
            if (type != other.type) return false
            if (header != other.header) return false
            if (banner != other.banner) return false
            if (hasCloseButton != other.hasCloseButton) return false

            return true
        }

        override fun hashCode(): Int {
            var result = id.hashCode()
            result = 31 * result + groupId.hashCode()
            result = 31 * result + layout.hashCode()
            result = 31 * result + name.hashCode()
            result = 31 * result + items.contentHashCode()
            result = 31 * result + type.hashCode()
            result = 31 * result + header.hashCode()
            result = 31 * result + banner.hashCode()
            result = 31 * result + hasCloseButton.hashCode()
            return result
        }
    }

    data class Item(
        @Expose
        @SerializedName("id")
        val id: String = "",
        @Expose
        @SerializedName("backColor")
        val backColor: String = "",
        @Expose
        @SerializedName("price")
        val price: String = "0",
        @Expose
        @SerializedName("imageUrl")
        val imageUrl: String = "",
        @Expose
        @SerializedName("name")
        val name: String = "",
        @Expose
        @SerializedName("applink")
        val applink: String = "",
        @Expose
        @SerializedName("discount")
        val discount: String = "",
        @Expose
        @SerializedName("slashedPrice")
        val slashedPrice: String = "",
        @Expose
        @SerializedName("textColor")
        val textColor: String = "",
    )

    data class Header(
        @Expose
        @SerializedName("id")
        val id: String = "",
        @Expose
        @SerializedName("name")
        val name: String = "",
        @Expose
        @SerializedName("subtitle")
        val subtitle: String = "",
        @Expose
        @SerializedName("applink")
        val applink: String = "",
        @Expose
        @SerializedName("backColor")
        val backColor: String = "",
        @Expose
        @SerializedName("backImage")
        val backImage: String = "",
        @Expose
        @SerializedName("textColor")
        val textColor: String = ""
    )

    data class Banner(
        @Expose
        @SerializedName("id")
        val id: String = "",
        @Expose
        @SerializedName("title")
        val title: String = "",
        @Expose
        @SerializedName("description")
        val description: String = "",
        @Expose
        @SerializedName("backColor")
        val backColor: String = "",
        @Expose
        @SerializedName("applink")
        val applink: String = "",
        @Expose
        @SerializedName("textColor")
        val textColor: String = "",
        @Expose
        @SerializedName("imageUrl")
        val imageUrl: String = "",
        @SerializedName("gradientColor")
        val gradientColor: ArrayList<String> = arrayListOf()
    )
}