package com.frenzelts.visitorpattern.data.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class HomeCaroussData(
    @SerializedName("homeDynamicContents")
    val dynamicHomeContent: DynamicHomeContent = DynamicHomeContent()
)