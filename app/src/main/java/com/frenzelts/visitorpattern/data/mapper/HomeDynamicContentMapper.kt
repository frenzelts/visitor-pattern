package com.frenzelts.visitorpattern.data.mapper

import android.util.Log
import com.frenzelts.visitorpattern.common.Visitable
import com.frenzelts.visitorpattern.data.model.*
import com.frenzelts.visitorpattern.view.adapter.model.dynamic_content.CarouselModel
import com.frenzelts.visitorpattern.view.adapter.model.dynamic_content.Grid4Model

class HomeDynamicContentMapper {
    private var visitableList: MutableList<Visitable<*>> = mutableListOf()

    fun mapDynamicContentToVisitableList(contents: List<DynamicHomeContent.Content>) : List<Visitable<*>>{
        visitableList.clear()
        contents.mapIndexed { index, content ->
            val position = index + 1
            when(content.layout){
                DynamicHomeContent.Content.LAYOUT_GRID4 -> createGrid4Component(content, position)
                DynamicHomeContent.Content.LAYOUT_CAROUSEL -> createCarouselComponent(content, position)
            }
        }
        return visitableList
    }

    private fun createGrid4Component(
        content: DynamicHomeContent.Content,
        position: Int
    ) {
        visitableList.add(
            Grid4Model(
                contentModel = DynamicContentComponentMapper.mapHomeContentToComponent(
                    content,
                    position
                )
            )
        )
    }

    private fun createCarouselComponent(
        content: DynamicHomeContent.Content,
        position: Int
    ) {
        visitableList.add(
            CarouselModel(
                contentModel = DynamicContentComponentMapper.mapHomeContentToComponent(
                    content,
                    position
                )
            )
        )
    }

}

object DynamicContentComponentMapper{
    fun mapHomeContentToComponent(
        content: DynamicHomeContent.Content,
        position: Int
    ): ContentModel {
        return ContentModel(
            id = content.id,
            name = content.name,
            groupId = content.groupId,
            type = content.type,
            layout = content.layout,
            verticalPosition = position,
            contentHeader = ContentHeader(
                content.header.id,
                content.header.name.ifBlank { content.banner.title },
                content.header.subtitle.ifBlank { content.banner.description },
                content.header.applink.ifBlank { content.banner.applink },
                content.header.backColor,
                content.header.backImage,
                content.header.textColor
            ),
            contentBanner = ContentBanner(
                id = content.banner.id,
                title = content.banner.title,
                description = content.banner.description,
                backColor = content.banner.backColor,
                applink = content.banner.applink,
                textColor = content.banner.textColor,
                imageUrl = content.banner.imageUrl,
                gradientColor = content.banner.gradientColor
            ),
            contentItems = content.items.map {
                ContentItem(
                    id = it.id,
                    price = it.price,
                    imageUrl = it.imageUrl,
                    name = it.name,
                    applink = it.applink,
                    discount = it.discount,
                    slashedPrice = it.slashedPrice,
                    backColor = it.backColor,
                    textColor = it.textColor,
                )
            }
        )
    }
}