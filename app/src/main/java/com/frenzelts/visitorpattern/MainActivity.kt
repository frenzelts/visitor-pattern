package com.frenzelts.visitorpattern

import android.os.Bundle
import android.util.Log
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.AsyncDifferConfig
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.frenzelts.visitorpattern.data.repository.HomeDynamicContentRepository
import com.frenzelts.visitorpattern.databinding.ActivityMainBinding
import com.frenzelts.visitorpattern.di.createWithFactory
import com.frenzelts.visitorpattern.view.HomeViewModel
import com.frenzelts.visitorpattern.view.adapter.HomeAdapter
import com.frenzelts.visitorpattern.view.adapter.HomeDiffCallback
import com.frenzelts.visitorpattern.view.adapter.typefactory.HomeTypeFactoryImpl
import java.util.concurrent.Executors

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    private val viewModel: HomeViewModel by viewModels {
        createWithFactory {
            HomeViewModel(repository = HomeDynamicContentRepository(application))
        }
    }
    private val asyncDifferConfig by lazy {
        AsyncDifferConfig.Builder(HomeDiffCallback)
            .setBackgroundThreadExecutor(Executors.newSingleThreadExecutor())
            .build()
    }
    private lateinit var homeAdapter: HomeAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        viewModel.fetchDynamicContent()

        setupRecyclerView()
        observeDynamicContent()
    }

    private fun setupRecyclerView(){
        homeAdapter = HomeAdapter(
            asyncDifferConfig,
            HomeTypeFactoryImpl(
                binding.rvHome.recycledViewPool
            ),
        )
        binding.rvHome.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context)
            adapter = homeAdapter
        }
    }

    private fun observeDynamicContent(){
        viewModel.homeLiveDynamicContent.observe(this){
            homeAdapter.submitList(it.list)
        }
    }
}